from Tkinter import *
from tkMessageBox import *
from win32api import GetSystemMetrics
import Acquisition_rest_position as aquisition
import os, time, subprocess, win32gui, win32con

global file_name_data, session

original_dir = os.getcwd()

def refresh_session():
    os.chdir(original_dir)
    ## Refresh folder ListBox
    folders = os.listdir("data/")
    num_folders = len(folders)
    Subject_List.delete(0,END)
    fold_count = 0
    if num_folders != 0:
        for fold in folders:
            fold_count = fold_count + 1
            Subject_List.insert(fold_count, fold)
    else:
        Subject_List.insert(1, "Empty")

    ## Refresh session ListBox
    if entry_user_id.get() != '':
        session_dir = "data/" + entry_user_id.get()+'/'
        sessions = os.listdir(session_dir)
        num_sessions = len(sessions)
        Session_List.delete(0,END)
        sessions_count = 0
        if num_sessions != 0:
            for sess in sessions:
                sess=os.path.splitext(sess)[0] #gives only the basename of the filename.txt
                sessions_count = sessions_count + 1
                Session_List.insert(sessions_count, sess)
        else:
            Session_List.insert(1, "Empty")

def create_session():
    global session
    time_stamp=time.strftime('%m-%d-%y_%H-%M')
    user = entry_user_id.get()
    session = entry_session.get()
    refresh_session()
    #path_user = 'data/' + id + '/'
    file_name_data = data_path="data/" + user + "/"+ session + "_" + user + "_" + time_stamp + ".txt"
    if session != '' and user in os.listdir('data/'):
        f = open(data_path,'w')
        f.close()
        print data_path + ' created'
    else:
        print 'Problem: Please check the session name or the user id'

def create_id_folder():
    os.chdir(original_dir)
    id = entry_user_id.get()
    if id in os.listdir('data/'):
        print "This subject is already registered"
    else:
        os.mkdir('data/'+id)
        print "The new subject %s has been  registered" %id

def Aquisition_rest_position():

    message = aquisition.aquisition_rest_position('workspace/rest_position.txt')
    print message

def calibration():
    global process
    process_name = "calibration/Controller_Leap.py"
    arguments=[]
    process = subprocess.Popen(["python", process_name])
    
def Free_Movement():
    global process
    process_name = "Tasks/Free_Movement/Controller_Leap.py"
    process = subprocess.Popen(["python", process_name])
    time.sleep(5)
    
def Continous_Reaching():
    global process
    process_name = "Tasks/Continous_Reaching/Controller_Leap.py"
    process = subprocess.Popen(["python", process_name])
    time.sleep(5)
    
def Center_Out_Reaching():
    global process
    refresh_parameters()
    process_name = "/Tasks/2D_reaching_2D_ball/Controller_Leap.py"
    the_cwd=os.getcwd()
    process = subprocess.Popen("python " + the_cwd + process_name, shell=True) 
    time.sleep(2)
    
def kill_subprocess():
    global process
    process.kill()
    os.system('TASKKILL /f /im H3DLoad.exe')

def refresh_parameters():

    EA_factor = str(EA_factor_scale.get())
    Target_factor = str(Target_factor_scale.get())
    Distance_to_target = str(Target_Zone_Size.get())
    Block_Length = str(Block_Length_entry.get()) #input in minutes 
    Trial_Length = str(Trial_Length_entry.get()) #input in seconds
    hand_impaired_str = str(hand_impaired.get())
    friction_coeff = str(Sphere_friction_scale.get())

    # if askyesno('Verify', 'Really quit?'):
    #     showwarning('Yes', 'Not yet implemented')
    # else:
    #     showinfo('No', 'Quit has been cancelled')

    parameter_file = open('workspace/parameters.txt', 'w')
    parameter_file.write("EA_factor,%s\nTarget_factor,%s\nDistance_to_target,%s\nBlock_Length,%s\nTrial_Length,%s\nhand_impaired_str,%s\nfriction_coeff,%s\n" % (EA_factor, Target_factor, Distance_to_target, Block_Length, Trial_Length, hand_impaired_str, friction_coeff))
    parameter_file.close()
    print EA_factor, "\n", Target_factor,"\n",Distance_to_target, "\n",Block_Length, "\n",Trial_Length,"\n",hand_impaired_str,"\n",friction_coeff 
    time_stamp=time.strftime('%m-%d-%y_%H-%M')
    print "SELECTION"+str(Subject_List.curselection())
    if len(Subject_List.curselection())!=0:
        user=str(Subject_List.get(Subject_List.curselection()))
        if len(Session_List.curselection())!=0:
            session=str(Session_List.get(Session_List.curselection()))
            print session
            data_path="data/" + user + "/"+ session + "_" + user + "_" + time_stamp + ".txt"
            print data_path
    else:
        if "default" not in os.listdir('data/'):
            os.mkdir("data/default")
        data_path=os.getcwd()+"/data/default/" + time_stamp + ".txt"
    datapath_filepath='resources/datapath_filepath.txt'
    file_path=open(datapath_filepath,'w')
    file_path.write(data_path)
    file_path.close()

def Toggle_Advanced_Buttons():
    pass
    
root = Tk()
## First part: User and folder settings

toolbar = Frame(root, bg="blue")
toolbar.pack(side=TOP, fill=X)

user_id_label = Label(toolbar, text ="ID")
session = Label(toolbar, text = "session")

user_id = StringVar()
entry_user_id = Entry(toolbar,text=user_id)
entry_session = Entry(toolbar)

user_id_label.grid(row=0, sticky=E)
session.grid(row=1,sticky=E)

entry_user_id.grid(row=0, column=1)
entry_session.grid(row=1, column=1)

CreateSubjectButt = Button(toolbar, text="Create Subject folder", command=create_id_folder)
CreateSubjectButt.grid(row=0,column= 2)

RefreshBut = Button(toolbar, text="Refresh", command=refresh_session)
RefreshBut.grid(row=0,column= 3)

CreateButt = Button(toolbar, text="Create Session", command=create_session)
CreateButt.grid(row=1,column= 2)

## folders handling
Folder_frame = Frame(root, bg="green")

Folder_frame.pack()
Subject_List = Listbox(Folder_frame,width=40, height=15, exportselection=0, selectmode=EXTENDED) #exportselction=0 makes it so that you can select from more than one listbox at once
Subject_List.grid(row=0,column=0)
folders = os.listdir("data/")
Subject_List.bind("<Double-Button-1>",refresh_session())

# entry_user_id.bind('<Double-1>',user_id.set("Francois"))

## Sessions list
Session_List = Listbox(Folder_frame,width=40, height=15,exportselection=0)
Session_List.grid(row=0, column=1)
# sessions = os.listdir("data/")
refresh_session()


## Sessions definitions
toolbar2 = Frame(root)
toolbar2.pack(padx=5, pady=10, side=LEFT)

toolbar2_title = Label(toolbar2, text="Action & Session",font=14)
toolbar2_title.pack(padx=2, pady=2)

calibration_button = Button(toolbar2, text="Calibration", command=calibration)
calibration_button.pack(padx=2, pady=2)

rest_position = Button(toolbar2, text="Rest Position Measurement", command=Aquisition_rest_position)
rest_position.pack(padx=2, pady=2)

# ROM = Button(toolbar2, text="Free Exploration", command=Free_Movement)
# ROM.pack(padx=2, pady=2)

# continuous_reaching = Button(toolbar2, text="Continuous reaching", command=Continous_Reaching)
# continuous_reaching.pack(padx=2, pady=2)

reaching2d_ball2d = Button(toolbar2, text="Center-Out Reaching", command=Center_Out_Reaching)
reaching2d_ball2d.pack(padx=2, pady=2)

stop_task = Button(toolbar2, text="Stop Task", command=kill_subprocess)
stop_task.pack( padx=2, pady=2)



# ******* Tuning of the parameters *******

global Block_Length, Trial_Length, EA_factor, Target_factor,Distance_to_target, friction_coeff
#default paramters
EA_factor =0
Target_factor = 0.15
Distance_to_target = 0.02 #m
Block_Length = 5 #minutes
Trial_Length = 10 #seconds (assuming a minimum of 100 trails at 2100 seconds/100 trails )
friction_coeff = 25

parameter_frame = Frame(root,width=200,height=100)
parameter_frame.pack(padx=5, pady=10, side=LEFT)
parameter_frame_title = Label(parameter_frame, text="Session parameters",font=14)
parameter_frame_title.grid(row=0,columnspan=4,padx = 2,pady = 4)
## Error Augmentation

EA_factor_scale = Scale( parameter_frame,variable= EA_factor, label = 'Error Augmentation factor', orient=HORIZONTAL , length = 200,
               tickinterval = 0.5, resolution = 0.1, from_ = -1, to = 2)
EA_factor_scale.set(EA_factor)
EA_factor_scale.grid(row=1,columnspan=2,padx = 2,pady = 4)


## Distance from home to target factor
Target_factor_scale = Scale( parameter_frame,variable= Target_factor, label = 'Target factor', orient=HORIZONTAL , length = 200,
               tickinterval = 0.1, resolution = 0.01, from_ = 0.15, to = 0.4) #, activebackground = 'red', troughcolor = 'blue'
                #target_factor max must be .4 bc otherwise the leap might not detect
Target_factor_scale.set(Target_factor)
Target_factor_scale.grid(row=2,columnspan=2,padx = 2,pady = 4)

## Distance to hit the target
Target_Zone_Size = Scale( parameter_frame,variable= Distance_to_target, label = 'Zone of target', orient=HORIZONTAL , length = 200,
               tickinterval = 0.01, resolution = 0.005, from_ = 0, to = 0.04) #, activebackground = 'red', troughcolor = 'blue'

Target_Zone_Size.set(Distance_to_target)
Target_Zone_Size.grid(row=3,columnspan=2,padx = 2,pady = 4)


## Time of a session

Block_Length_entry = Entry(parameter_frame)
Block_Length_entry.insert(END,Block_Length)
Block_Length_label = Label(parameter_frame,text="Block Length (minutes)")
Block_Length_label.grid(row=4,column=1,padx = 2,pady = 4)
Block_Length_entry.grid(row=4,column=0, padx = 2,pady = 4)


## Time of the trial
Trial_Length_entry = Entry(parameter_frame)
Trial_Length_entry.insert(END,Trial_Length)
Trial_Length_label = Label(parameter_frame,text="Trial Length (seconds)")
Trial_Length_label.grid(row=4,column=2,padx = 2,pady = 4)
Trial_Length_entry.grid(row=4,column=3,padx = 2,pady = 4)

## Side to treat
hands_impaired = ['none', 'left', 'right']
hand_impaired = StringVar(root)
hand_impaired.set('None')

## Sphere Friction

Sphere_friction_scale = Scale( parameter_frame,variable= friction_coeff, label = 'Sphere Physic', orient=HORIZONTAL , length = 200,
               tickinterval = 5, resolution = 5, from_ = 10, to = 50) #, activebackground = 'red', troughcolor = 'blue'

Sphere_friction_scale.set(friction_coeff)
Sphere_friction_scale.grid(row=3,column=2,columnspan=2,padx = 2,pady = 4)
print friction_coeff


hand_choice_label = Label(parameter_frame,text="Side affected ", font=10)
hand_choice = OptionMenu(parameter_frame, hand_impaired, *hands_impaired)
hand_choice_label.grid(row=1,column=2,columnspan=2,padx = 2,pady = 4)
hand_choice.grid(row=2,column=2,columnspan=2,padx = 2,pady = 4)

refresh_parameters_button = Button(parameter_frame, text="Refresh parameters", font = 12,command=refresh_parameters)
refresh_parameters_button.grid(row=6,columnspan=4)

root.mainloop()
