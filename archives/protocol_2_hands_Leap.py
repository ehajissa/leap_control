# Insert Trial Schedule Here
# from simple_settings.py import *

# Insert Trial Schedule Above
# Start Experiment after the following trial number

from H3DInterface import *
from H3D import *
from math import (ceil, floor, atan, sqrt, sin, cos, pow, atan2)
import numpy as np

## Variables for Phase and Trial Settings

global Target_Positions, number_targets
Target_Positions = np.genfromtxt('target_position.txt', delimiter =',')
number_targets = len(Target_Positions)

casereq_last = -1
casereq = 0
Trial_random_pick = 0
CaseTimer = 0

ExperimentComplete = 0

filename = 'RenameMe_Data.txt'
f = open(filename, 'a')


def initialize():
    # initialize vectors of length(trials) for settings
    print "initialize"
    global Target_Positions
    global TimeInitial

    TimeInitial = references.getValue()[0].cycleTime.getValue()
    Header = "Initial time %s\n Target position %s\n" %(TimeInitial,TargetPositions)
    data_labels = "time   TrialCounter  Casereq    Left_x  Left_y  Left_z  Right_x Right_y Right_z Tray_x  Tray_y  Tray_z  Tray_rot_x  Tray_rot_y  Tray_rot_z  Tray_angle  Sphere_x    Sphere_y   Sphere_z"
    f.write('%s \n' % (data_labels))
    return 0


class State_Detection(AutoUpdate(TypedField(MFBool, (SFTime)))):
    def update(self, event):
        # print "State detection"
        global TimeInitial
        routes_in = self.getRoutesIn()
        time = routes_in[0].getValue()
        time = time - TimeInitial
        # H3D relative 
        TimeSensor = references.getValue()[0]
        View_Point = references.getValue()[1].position.getValue()
        # Actual measurements of the leap
        Leap_left_transl = references.getValue()[2].translation.getValue()
        Leap_right_transl = references.getValue()[3].translation.getValue()
        Velocity_left = references.getValue()[8].translation.getValue()
        Velocity_right = references.getValue()[9].translation.getValue()
        # Home point position
        Home_left_pos = references.getValue()[4].translation.getValue()
        Home_right_pos = references.getValue()[5].translation.getValue()
        # Target point position
        Target_left_pos = references.getValue()[6].translation.getValue()
        Target_right_pos = references.getValue()[7].translation.getValue()
        # Tray features
        Tray_pos = references.getValue()[10].translation.getValue()
        Tray_rot = references.getValue()[10].rotation.getValue()

        Speed_left = sqrt(pow(Velocity_left.x, 2)
                          + pow(Velocity_left.y, 2)
                          + pow(Velocity_left.z, 2))
        Speed_right = sqrt(pow(Velocity_right.x, 2)
                           + pow(Velocity_right.y, 2)
                           + pow(Velocity_right.z, 2))

        Home_Error_left = sqrt(pow(Leap_left_transl.x - Home_left_pos.x, 2)
                               + pow(Leap_left_transl.y - Home_left_pos.y, 2)
                               + pow(Leap_left_transl.z - Home_left_pos.z, 2))
        Home_Error_right = sqrt(pow(Leap_right_transl.x - Home_right_pos.x, 2)
                                + pow(Leap_right_transl.y - Home_right_pos.y, 2)
                                + pow(Leap_right_transl.z - Home_right_pos.z, 2))

        Target_Error_left = sqrt(pow(Leap_left_transl.x - Target_left_pos.x, 2)
                                 + pow(Leap_left_transl.y - Target_left_pos.y, 2)
                                 + pow(Leap_left_transl.z - Target_left_pos.z, 2))
        Target_Error_right = sqrt(pow(Leap_right_transl.x - Target_right_pos.x, 2)
                                  + pow(Leap_right_transl.y - Target_right_pos.y, 2)
                                  + pow(Leap_right_transl.z - Target_right_pos.z, 2))

        StateCode = [(Home_Error_left < 0.01) & (Home_Error_right < 0.01),
                     # 1 start condition for the cycle of reaching
                     (Home_Error_left > 0.01) & (Home_Error_right > 0.01)
                     & (Target_Error_left > 0.01) & (Target_Error_right > 0.01),  # 2 in cycle reaching condition
                     (Speed_left < 0.1) & (Speed_right < 0.1),  # 3 No motion
                     (Target_Error_left < 0.01) & (
                     Target_Error_right < 0.01)]  # 4 completation condition for the cycle of reaching
        return StateCode


class Protocol_Control(AutoUpdate(TypedField(SFInt32, (MFBool)))):  # protocol
    def update(self, event):
        ## State information calculated
        routes_in = self.getRoutesIn()
        StateCode = routes_in[0].getValue()

        In_Home = StateCode[0]
        In_Movement = StateCode[1]
        No_Motion = StateCode[2]
        In_Target = StateCode[3]

        ## Time variable definitions
        TimeSensor = references.getValue()[0]
        global TimeInitial
        time = TimeSensor.cycleTime.getValue()
        time = time - TimeInitial

        ## Functional varaibles definitions
        global casereq
        global casereq_last
        global Trial_random_pick
        global number_targets
        global ExperimentComplete

        ######## Check if the time is runing
        def case_initial():  # case 0
            if time < 0:
                casereq = 0
            else:
                print "------ Trial started -------"
                casereq = 1
            return casereq

        ########
        def case_loadparam_wait():  # case 1
            global TrialCounter
            if (In_Home & No_Motion):
                TrialCounter = TrialCounter + 1
                print "Start of the trial : %d" % (TrialCounter)
                casereq = 3
            else:
                casereq = 1
            return casereq

        ########
        def case_waitingforstop():  # case 3
            global CaseTimer
            global breakevent
            global TrialCounter
            CaseTimer = CaseTimer + 1
            if (In_Target & No_Motion):
                casereq = 1
            else:
                casereq = 3
            return casereq

        ########
        def case_winddown():  # case 4
            global CaseTimer
            CaseTimer = 0
            global breakevent
            breakevent = 0
            if (No_Motion):
                casereq = 1
            else:
                casereq = 4
            if TrialCounter == maxtrials + 1:
                casereq = 5
            return casereq

        ########
        def case_end():  # case 5
            print "End of the trials"
            casereq = 5
            return casereq

        ####### Begining of the real process
        pcase = {0: case_initial,
                 1: case_loadparam_wait,
                 3: case_waitingforstop,
                 4: case_winddown,
                 5: case_end}

        casereq = pcase[casereq]()

        if TrialCounter == number_trials + 1:
            casereq = 5
            TrialCounter = 1
            ExperimentComplete = 1
        casereq_last = casereq
        return casereq


class Set_Target_Position(TypedField(SFVec3f, (SFInt32))):
    def update(self, event):
        global Trial_random_pick
        global tar_x
        global tar_y
        global tar_z
        routes_in = self.getRoutesIn()
        case = routes_in[0].getValue()
        px = Target_Positions[TrialCounter - 1, 0]
        py = Target_Positions[TrialCounter - 1, 1]
        pz = Target_Positions[TrialCounter - 1, 2]
        if casereq == 3:
            Target_left_pos = Vec3f(px, py, pz)
            Target_right_pos = Vec3f(px + 0.2, py, pz)
        else:
            Target_left_pos = Vec3f(10, 10, 10)
            Target_right_pos = Vec3f(10, 10, 10)
        references.getValue()[6].translation.setValue(Target_left_pos)
        references.getValue()[7].translation.setValue(Target_right_pos)

        return Target_left_pos


class Set_Home_Position(AutoUpdate(TypedField(SFVec3f, (SFInt32)))):
    def update(self, event):
        routes_in = self.getRoutesIn()
        case = routes_in[0].getValue()
        # print "home case", case, "bool ", any([0,1,2])
        global Trial_random_pick
        Home_left_pos = references.getValue()[4]
        Home_right_pos = references.getValue()[5]
        Instruction = references.getValue()[11]
        # print Instruction.string.getValue()
        # Instruction.setValue(SFString("Yolo"))
        if case in [0, 1, 2, 4]:
            home_pos_left = Vec3f(-0.2, 0.4, 0)
            Home_right_pos.translation.setValue(Vec3f(0.2, 0.4, 0))
            Instruction.string.setValue(["Reach the", "rest position"])
        elif case == 3:
            home_pos_left = Vec3f(10, 10, 10)
            Instruction.string.setValue(["Reach the target"])
            # The previous line is equivalent to:
            # Home_left_pos.translation.setValue(Vec3f(10,10,10))
            Home_right_pos.translation.setValue(Vec3f(10, 10, 10))
        elif case == 5:
            home_pos_left = Vec3f(10, 10, 10)
            Instruction.string.setValue(["Trial done"])

        return home_pos_left


class Write_Data(AutoUpdate(TypedField(SFBool, (SFInt32)))):
    def update(self, event):
        global TimeInitial
        routes_in = self.getRoutesIn()
        casereq = routes_in[0].getValue()

        # H3D relative
        time = references.getValue()[0].cycleTime.getValue()
        print time
        time = time - TimeInitial
        View_Point = references.getValue()[1].position.getValue()
        # Actual measurements of the leap
        Leap_left_transl = references.getValue()[2].translation.getValue()
        Leap_right_transl = references.getValue()[3].translation.getValue()
        Velocity_left = references.getValue()[8].translation.getValue()
        Velocity_right = references.getValue()[9].translation.getValue()
        # Home point position
        Home_left_pos = references.getValue()[4].translation.getValue()
        Home_right_pos = references.getValue()[5].translation.getValue()
        # Target point position
        Target_left_pos = references.getValue()[6].translation.getValue()
        Target_right_pos = references.getValue()[7].translation.getValue()
        # Tray features
        Tray_pos = references.getValue()[10].translation.getValue()
        Tray_rot = references.getValue()[10].rotation.getValue()
        Sphere_pos = references.getValue()[12].translation.getValue()

        global Trial_random_pick

        if casereq != 0 and Leap_left_transl.x != 0 and casereq != 5 and TrialCounter != 0:
            ## labels: "time   TrialCounter     Casereq    Left_x  Left_y  Left_z  Right_x Right_y Right_z Tray_x  Tray_y  Tray_z  Tray_rot_x  Tray_rot_y  Tray_rot_z  Tray_angle  Sphere_x    Sphere_y   Sphere_z"
            f.write('%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n' % (time, TrialCounter, casereq,
                                                  Leap_left_transl.x, Leap_left_transl.y, Leap_left_transl.z,
                                                  Leap_right_transl.x,Leap_right_transl.y,Leap_right_transl.z,
                                                  Tray_pos.x, Tray_pos.y, Tray_pos.z,
                                                  Tray_rot.x,Tray_rot.y,Tray_rot.z,Tray_rot.angle,
                                                  Sphere_pos.x, Sphere_pos.y, Sphere_pos.z))

        return 0


state_detection = State_Detection()
protocol = Protocol_Control()

target_position = Set_Target_Position()
home_position = Set_Home_Position()

write_data=Write_Data()
