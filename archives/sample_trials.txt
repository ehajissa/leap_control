# The TargetTrial class defines trials to be used in the TargetExperiment class.															
# It uses the following values from the experiment file:															
#															
# pos_x                     The position of the target in x															
# pos_y                     The position of the target in y															
# pos_z                     The position of the target in z															
# target_radius             The radius of the target															
# speed_threshold           The threshold under which the speed of the tracker 															
#                           must be for the trial to be completed 															
# target_hold_time          The time the tracker must be inside the radius of 															
#                           the target for the trial to be completed.															
# maximum_trial_time        The maximum time to show the target before moving on															
#                           to the next target.															
# target_pause_time         The time from completing the last trial until this 															
#                           trial is shown															
# feedback_time             The time that trial complete feedback will be shown 															
#                           before going to next target.															
# distance_force_function   The force function dependent on the distance from 															
"#                           the target. E.g. ""if(x<0.03, x*100, 3 )"" will give"															
#                           a force of 3 N of distance is > 0.03 and a spring 															
#                           pulling to the target position with stiffness 100 N/m															
#                           inside that distance. See GeneralFunction.h for 															
#                           information about how to define the function. The 															
#                           force direction will always be towards the center of the target.															
# use_force_impulse         If 1 force impulse will be used.															
# force_impulse_speed_threshold The speed the subject has to be below for an impulse force to start. 															
# force_impulse_time_below_threshold The time below the defined threshold															
# force_impulse_force       The force to apply(in N). The 															
#                           force direction will always be towards the center of the target.															
# force_impulse_time        The time to apply the force.															
"# Coordinate system: (0,0,0) is the center of the screen. The direction is on the serface of the screen that we see in the VRROOM."															
															
__ BEGIN:__															
trial	pos_x 	pos_y 	pos_z 	target_radius 	speed_threshold 	target_hold_time 	target_pause_time 	feedback_time 	maximum_trial_time 	distance_force_function	use_force_impulse 	force_impulse_force 	force_impulse_time 	force_impulse_speed_threshold 	force_impulse_time_below_threshold
1	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
2	 0		-0.1	 0.3	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
3	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
4	-0.35	-0.225	 0.05	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
5	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
6	 0.2	-0.225	 0.1	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
7	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
8	 0		-0.225	 0.1	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
9	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
10	-0.3	-0.1	 0.3	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
11	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
12	 0.25	-0.4	-0.0	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
13	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
14	 0		-0.4	-0.1	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
15	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
16	 0.15	-0.15	 0.3	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
17	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
18	-0.4	-0.4	-0.05	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5
19	 0		-0.5	 0.2	0.02	1	0.5	0.5	0.5	10	0	0	1	0.25	0.06	1.5