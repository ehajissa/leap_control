################################################################################
# Copyright (C) 2012-2013 Leap Motion, Inc. All rights reserved.               #
# Leap Motion proprietary and confidential. Not for distribution.              #
# Use subject to the terms of the Leap Motion SDK Agreement available at       #
# https://developer.leapmotion.com/sdk_agreement, or another agreement         #
# between Leap Motion and you, your company or other organization.             #
################################################################################

####### Modified by Francois Kade 2016

import os, sys, inspect, thread, time, struct
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))
os.system('H3DViewer protocol_leap_only_2_hands.x3d')
from socket import *
PORT_out = 12000
remoteHost_out = ''
sock_out = socket(AF_INET, SOCK_DGRAM)
sock_out.bind(('', PORT_out))

import numpy as np
import Leap
file = open('workfile.txt','w')

# print "OPENED"
time.sleep(1)

class SampleListener(Leap.Listener):
    finger_names = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    bone_names = ['Metacarpal', 'Proximal', 'Intermediate', 'Distal']
    state_names = ['STATE_INVALID', 'STATE_START', 'STATE_UPDATE', 'STATE_END']

    def on_init(self, controller):
        print "Initialized"

    def on_connect(self, controller):
        print "Connected"

    def on_disconnect(self, controller):
        print "Disconnected"

    def on_exit(self, controller):
        print "Exited"

    def on_frame(self, controller):
        global Hands
        # Get the most recent frame and report some basic information
        frame = controller.frame()
        Hands = frame.hands
        file.write("Frame_id,%d,timestamp,%d,hands,%d,fingers,%d" % (
            frame.id, frame.timestamp, len(frame.hands), len(frame.fingers)))
            
        print "Frame_id,%d,timestamp,%d,hands,%d,fingers,%d" % (
            frame.id, frame.timestamp, len(frame.hands), len(frame.fingers))
            
        for hand in frame.hands:
            
            handType = "left" if hand.is_left else "right"
            normal = hand.palm_normal
            direction = hand.direction
            arm = hand.arm
            
            file.write(",%s,id,%d,hand_position,%s,hand_direction,%s,hand_normal,%s,hand_velocity,%s" % (
                handType, hand.id, hand.palm_position, hand.direction,hand.palm_normal,hand.palm_velocity))
            print ",%s,id,%d,hand_position,%s,hand_direction,%s,hand_normal,%s,hand_velocity,%s" % (
                handType, hand.id, hand.palm_position, hand.direction,hand.palm_normal,hand.palm_velocity)

            if len(frame.hands) == 2:
                if hand.is_left:
                    hand_left_position = [hand.palm_position.x/1000, hand.palm_position.y/1000 ,  hand.palm_position.z/1000]
                    hand_left_velocity = [hand.palm_velocity.x/1000, hand.palm_velocity.y/1000 ,  hand.palm_velocity.z/1000]
                else:
                    hand_right_position = [hand.palm_position.x/1000 , hand.palm_position.y/1000,  hand.palm_position.z/1000]
                    hand_right_velocity = [hand.palm_velocity.x/1000, hand.palm_velocity.y/1000 ,  hand.palm_velocity.z/1000]
            elif len(frame.hands) == 1:
                hand_left_position = [hand.palm_position.x/1000, hand.palm_position.y/1000 ,  hand.palm_position.z/1000]
                hand_left_velocity = [hand.palm_velocity.x/1000, hand.palm_velocity.y/1000 ,  hand.palm_velocity.z/1000]
                hand_right_position = [hand.palm_position.x/1000, hand.palm_position.y/1000 ,  hand.palm_position.z/1000]
                hand_right_velocity = [hand.palm_velocity.x/1000, hand.palm_velocity.y/1000 ,  hand.palm_velocity.z/1000 ]
        if len(frame.hands) == 0:
            hand_left_position = [0.2, 0 ,  0]
            hand_left_velocity = [0, 0, 0]
            hand_right_position = [-0.2, 0 ,  0]
            hand_right_velocity = [0, 0, 0]
            
        middle_hand = [(hand_left_position[0] + hand_right_position[0])/2,
            (hand_left_position[1]+ hand_right_position[1])/2,
            (hand_left_position[2]+ hand_right_position[2])/2]
        if len(frame.hands) == 2:
            angle_tray = np.arctan((hand_left_position[1]-hand_right_position[1])/(hand_left_position[0]-hand_right_position[0]))
        else:
            angle_tray = 0
            
        msg_pack_in, address = sock_out.recvfrom(1024) #receving request from port
        msg_out = struct.pack("f f f f f f f f f f f f f f f f", 
            hand_left_position[0], hand_left_position[1], hand_left_position[2],    # position of the first hand
            hand_left_velocity[0], hand_left_velocity[1], hand_left_velocity[2],    # velocity of the first hand
            hand_right_position[0], hand_right_position[1], hand_right_position[2],    # position of the second hand
            hand_right_velocity[0], hand_right_velocity[1], hand_right_velocity[2],    # velocity of the second hand
            middle_hand[0],middle_hand[1],middle_hand[2],                # middle of the hands
            angle_tray)                                     # angle of rotation in radian
        sock_out.sendto(msg_out, address)
        
        ## Integration related variables
        previous_frame = frame.id
        previous_time = frame.timestamp
        file.write("\n")        




def main():
    # Create a sample listener and controller
    controller = Leap.Controller()
    listener = SampleListener()
    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print "Press Enter to quit..."
    try:
        sys.stdin.readline()
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)


if __name__ == "__main__":
    main()
