clear
close all

load('vector_position_right_hand.mat')

figure
hold on
title('Postion of the targets')
grid on
scatter3(0,0,0,'red')
scatter3(vector_position(:,1),vector_position(:,2), vector_position(:,3),'blue')

%% Calculation of the path
path_to_target = zeros(18,1)
for i = 1:length(vector_position)
    path_to_target(i) = norm(vector_position(i,:))
end
figure
[xq,yq] = meshgrid(linspace(min(vector_position(:,1)),max(vector_position(:,1)),10),...
                linspace(min(vector_position(:,2)),max(vector_position(:,2)),10))
vq = griddata(vector_position(:,1),vector_position(:,2),vector_position(:,3),xq,yq)

figure
surf(xq,yq,vq);
