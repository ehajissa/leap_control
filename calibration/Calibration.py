from Tkinter import *
import os
import numpy as np

def edit_calibration(angle_rot,shift_x,shift_y,shift_z):
    os.chdir('..')
    calibration = np.genfromtxt('calibration.txt', delimiter=',')
    f = open('calibration.txt', 'w')
    f.write("rotation_virtual_screen in radians,1,0,0,%s\n"
            "translation_virtual_screen in meters,%s,%s,%s,0"
            % ( calibration[0,4] + angle_rot,
                calibration[1,1] + shift_x,
                calibration[1,2] + shift_y,
                calibration[1,3] + shift_z))
    f.close()
    os.chdir('calibration')

def rotup():
    edit_calibration(0.01, 0, 0, 0)
def rotdown():
    edit_calibration(-0.01, 0, 0, 0)

def shiftleft():
    edit_calibration( 0,-0.01, 0, 0)
def shiftright():
    edit_calibration( 0,0.01, 0, 0)

def shiftin():
    edit_calibration( 0, 0,0.01, 0)
def shiftout():
    edit_calibration(0, 0,-0.01, 0)

def shiftup():
    edit_calibration(0, 0, 0, 0.01)
def shiftdown():
    edit_calibration(0, 0, 0, -0.01)


root = Tk()
frame = Frame(root,width=200,height=100)
frame.pack()

Up_button = Button(frame, text="Up",command= shiftup)
Down_button = Button(frame, text="Down",command= shiftdown)
Left_button = Button(frame, text="Left",command= shiftleft)
Right_button = Button(frame, text="Right",command= shiftright)
In_button = Button(frame, text="In",command= shiftin)
Out_button = Button(frame, text="Out",command= shiftout)
Rotate_up = Button(frame, text="Rot Up",command= rotup)
Rotate_down = Button(frame, text="Rot Down",command= rotdown)

Up_button.grid(row=0,column= 1)
Down_button.grid(row=2,column= 1)
Left_button.grid(row=1,column= 0)
Right_button.grid(row=1,column= 2)
In_button.grid(row=3,column= 0)
Out_button.grid(row=3,column= 1)
Rotate_up.grid(row=4,column= 0)
Rotate_down.grid(row=4,column= 1)

root.mainloop()