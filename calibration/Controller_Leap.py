import os, sys, inspect, struct, Leap, win32process
from socket import *
import numpy as np

#set up socket communication
from socket import *
sock_out = socket(AF_INET, SOCK_DGRAM)
sock_out.bind(('', 0)) #0 slects a random available port
port_out=sock_out.getsockname()[1] #get the port number to send to the receiver
port_file = open("port_number.txt", 'w') #open (and make a new file) for writing
port_file.write("port number,%s" %str(port_out))
port_file.close()

# **START H3D with specified windows process settings **
root_dir = os.getcwd() # Open the x3d file (NB The default x3d file program should be H3DViewer bin32)
process_name=root_dir+'//calibration//visualization.x3d' 
si=win32process.STARTUPINFO()
si.dwFlags = win32process.STARTF_USEPOSITION #flag to use dwX and dwY
si.dwX = 0 #*
si.dwY = -1080 # together put window in upper left of TV screen (if Display Settings is setup properly)
# params=dict()
# params['startupinfo'] = si
processSecurityAttributes = None
threadSecurityAttributes = None
fInheritHandles = 0
creationFlags = win32process.CREATE_DEFAULT_ERROR_MODE #makes python scripts called by H3D output errors to the cmd
environment = None
currentDirectory = None
win32process.CreateProcess(None, 'H3DLoad '+process_name, None, None, 1, creationFlags,None, None ,si)

#*** change the path to import a custom library
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))
sys.path.append(os.getcwd())
import Coordinate_transformation #custom lib


os.chdir('calibration')
os.startfile('Calibration.py')
os.chdir("..")



# print os.getcwd()
# os.startfile('visualization.x3d')




Leap_transformation = Coordinate_transformation.eulerAnglesToRotationMatrix()
print Leap_transformation

controller = Leap.Controller()
## Enable background apps for the Leap
controller.set_policy(Leap.Controller.POLICY_BACKGROUND_FRAMES)

while True:

    frame = controller.frame()
    Hands = frame.hands

    for hand in Hands:

        # hand_position = np.dot(Leap_transformation, [hand.palm_position.x / 1000, hand.palm_position.y / 1000, hand.palm_position.z / 1000, 1])
        # hand_velocity = np.dot(Leap_transformation, [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000, hand.palm_velocity.z / 1000, 0])
        
        hand_position =[hand.palm_position.x / 1000, hand.palm_position.y / 1000, hand.palm_position.z / 1000, 1]
        hand_velocity =[hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000, hand.palm_velocity.z / 1000, 0]
        
        if len(frame.hands) == 2:
            if Hands.leftmost==hand: #is_left:
                hand_left_position = hand_position
                hand_left_velocity = hand_velocity
            else:
                hand_right_position = hand_position
                hand_right_velocity = hand_velocity
        elif len(frame.hands) == 1:
            hand_left_position = hand_position
            hand_left_velocity = hand_velocity
            hand_right_position =hand_position
            hand_right_velocity = hand_velocity
    if len(frame.hands) == 0:
        hand_left_position =[-0.2, 0, 0, 1]
        hand_left_velocity = [0, 0, 0, 0]
        hand_right_position =[0.2, 0, 0, 1]
        hand_right_velocity =[0, 0, 0, 0]

    middle_hand = [(hand_left_position[0] + hand_right_position[0]) / 2,
                   (hand_left_position[1] + hand_right_position[1]) / 2,
                   (hand_left_position[2] + hand_right_position[2]) / 2]
    if len(frame.hands) == 2:
        angle_tray = -  np.arctan((hand_right_position[2] - hand_left_position[2]) / (hand_right_position[0] - hand_left_position[0]))
    else:
        angle_tray = 0
    #print str(hand_left_position[0])+str(hand_left_position[1])+str(hand_left_position[2])
    msg_pack_in, address = sock_out.recvfrom(1024)  # receving request from port
    msg_out = struct.pack("f f f f f f f f f f f f f f f f",
                          hand_left_position[0], hand_left_position[1], hand_left_position[2],
                          # position of the first hand
                          hand_left_velocity[0], hand_left_velocity[1], hand_left_velocity[2],
                          # velocity of the first hand
                          hand_right_position[0], hand_right_position[1], hand_right_position[2],
                          # position of the second hand
                          hand_right_velocity[0], hand_right_velocity[1], hand_right_velocity[2],
                          # velocity of the second hand
                          middle_hand[0], middle_hand[1], middle_hand[2],  # middle of the hands
                          angle_tray)  # angle of rotation in radian
    sock_out.sendto(msg_out, address)
    
    print str([hand_left_position[0], hand_left_position[1], hand_left_position[2]])