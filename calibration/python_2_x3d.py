from H3DInterface import *
from H3D import *
from math import (ceil, floor, atan, sqrt, sin, cos, pow, atan2,radians)
import numpy as np
## Reciever
from socket import *
import struct, os
import numpy as np

## Reciever
from socket import *
port_number=int(np.genfromtxt('port_number.txt', delimiter =',')[1]) #get the port number that was set by Conroller_Leap.py
print "PORT NUMBER"
print port_number
HOST, PORT = "127.0.0.2", port_number
sock_in = socket(AF_INET, SOCK_DGRAM)
message='request'
print "pro port %s" %PORT


def initialize():
    global Rest_Positions
    Rest_Positions = np.genfromtxt('workspace/rest_position.txt', delimiter = ',')
    os.chdir('calibration')
    return 0


count=0
class Set_Hand_Position(AutoUpdate(TypedField(SFInt32, (SFTime)))):
    def update(self, event):
        global Rest_Positions
        global count
        count = count+1 

        Hand_left = references.getValue()[0]
        Hand_right = references.getValue()[1]

        ## Data exchange with Controller_Leap.py
        sock_in.sendto(message, (HOST, PORT))  # asks for one piece of data per scene graph loop cycle
        data, addr = sock_in.recvfrom(1024)
        numOfValues = len(data) / 4
        unp_data = struct.unpack('@' + 'f' * numOfValues, data)

        hand_left_position = np.array(unp_data[0:3])
        hand_right_position = np.array(unp_data[6:9])

        Hand_left.getField("translation").setValue(Vec3f(hand_left_position[0],hand_left_position[1],hand_left_position[2]))
        Hand_right.getField("translation").setValue(Vec3f(hand_right_position[0],hand_right_position[1],hand_right_position[2]))
        
        if np.mod(count,60) == 0:
            print 
            print count
            print hand_left_position
            print hand_right_position
        
        return 1

class Set_Home(AutoUpdate(TypedField(SFInt32, (SFTime)))):
    def update(self, event):
        global Rest_Positions
        Home_left = references.getValue()[2]
        Home_right = references.getValue()[3]

        Home_left.getField("translation").setValue(
            Vec3f(Rest_Positions[0, 0], Rest_Positions[0, 1], Rest_Positions[0, 2]))
        Home_right.getField("translation").setValue(
            Vec3f(Rest_Positions[1, 0], Rest_Positions[1, 1], Rest_Positions[1, 2]))
        return 1

class Set_Target(AutoUpdate(TypedField(SFInt32, (SFTime)))):
    def update(self, event):
        global Rest_Positions
        Home_left = references.getValue()[4]
        Home_right = references.getValue()[5]

        Home_left.getField("translation").setValue(
            Vec3f(Rest_Positions[0, 0], Rest_Positions[0, 1], Rest_Positions[0, 2]+0.2))
        Home_right.getField("translation").setValue(
            Vec3f(Rest_Positions[1, 0], Rest_Positions[1, 1], Rest_Positions[1, 2]+0.2))
        return 1

class Set_Environement(AutoUpdate(TypedField(SFInt32, (SFTime)))):
    def update(self, event):

        Rotation_screen = references.getValue()[6]
        Translation_screen = references.getValue()[7]

        # print
        # print Rotation_screen.translation.getValue()
        # print Translation_screen.translation.getValue()

        ### Read the settings files
        global calibration
        os.chdir('..')
        calibration = np.genfromtxt('calibration.txt', delimiter=',')
        os.chdir('calibration')
        Rotation_screen.getField("rotation").setValue(Rotation(1,0, 0, radians(calibration[0,4])))
        Translation_screen.getField("translation").setValue(Vec3f(calibration[1,1], calibration[1,2], calibration[1,3]))

        return 1

hand_positions = Set_Hand_Position()
home_position = Set_Home()
target_position = Set_Target()
environment = Set_Environement()