from H3DInterface import *
import numpy as np   
import time
global k
global Old_time, Old_velocity, Old_position_perp
k = 1
Old_time = 0.0
Old_velocity = 0.0
Old_position_perp = 0.0
        
class Set_Sphere_Position(AutoUpdate(TypedField( SFVec3f,(SFTime) )) ):
    def update(self, event):
        global k
        global Old_time, Old_velocity, Old_position_perp
        
        ## Reference values definition
        sphere_velocity,sphere_balance,tray = references.getValue()
        ball_velocity =  sphere_velocity.translation.getValue()
        ball_position =  sphere_balance.translation.getValue()
        tray_position = tray.translation.getValue()
        tray_rot = tray.rotation.getValue().angle

        ## Simulation of the physic
        g = 3
        friction = 0.8
        Actual_time = time.time()
        time_inc = Actual_time - Old_time
        
        acc_perp_tray = - g*np.sin(tray_rot)*np.cos(tray_rot)
        vel_perp_tray = Old_velocity + acc_perp_tray*time_inc 
        pos_perp_tray = Old_position
        
        if pos_perp_tray < -0.5:
            pos_perp_tray = -0.5
            vel_perp_tray = 0
        elif pos_perp_tray > 0.5:
            pos_perp_tray = 0.5
            vel_perp_tray = 0
        else:
            pos_perp_tray = ((1/2)*acc_perp_tray*time_inc*time_inc)+ vel_perp_tray*time_inc + pos_perp_tray
        # print pos_perp_tray
        return_ball_position = Vec3f(tray_position.x + pos_perp_tray*np.cos(tray_rot),
            tray_position.y+ 0.06 + pos_perp_tray*np.sin(tray_rot),
            tray_position.z)  
        # print "Old Time:", Old_time
        # print "Actual time", Actual_time
        # print "Increment  ", time_inc
        # print "Done"
        sphere_balance.getField("translation").setValue(return_ball_position)
        Old_time = Actual_time
        Old_velocity = vel_perp_tray
        Old_position = pos_perp_tray
        return return_ball_position            
            
       
    
sphere_position = Set_Sphere_Position() 
