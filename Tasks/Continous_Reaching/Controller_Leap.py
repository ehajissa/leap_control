import os, sys, inspect, thread, time, struct
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
import numpy as np

## Open the x3d file (NB The default x3d file program should be H3DViewer bin32)
root_dir = os.getcwd()
os.startfile(root_dir+'/Tasks/Continous_Reaching/visualization.x3d')
# os.startfile('visualization.x3d')

sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))
import Leap

from socket import *
PORT_out = 12000
remoteHost_out = ''
sock_out = socket(AF_INET, SOCK_DGRAM)
sock_out.bind(('', PORT_out))


controller = Leap.Controller()
## Enable background apps for the Leap
controller.set_policy(Leap.Controller.POLICY_BACKGROUND_FRAMES)

count = 0
while True:
    count = count + 1
    if np.mod(count,2) ==1:
        print
    else:
        pass
    # print controller.POLICY_BACKGROUND_FRAMES
    frame = controller.frame()
    Hands = frame.hands
    # file.write("Frame_id,%d,timestamp,%d,hands,%d,fingers,%d" % (
    #     frame.id, frame.timestamp, len(frame.hands), len(frame.fingers)))

    print "Frame_id,%d,timestamp,%d,hands,%d,fingers,%d" % (
        frame.id, frame.timestamp, len(frame.hands), len(frame.fingers))

    for hand in frame.hands:

        handType = "left" if hand.is_left else "right"
        normal = hand.palm_normal
        direction = hand.direction
        arm = hand.arm

        # file.write(",%s,id,%d,hand_position,%s,hand_direction,%s,hand_normal,%s,hand_velocity,%s" % (
        #     handType, hand.id, hand.palm_position, hand.direction, hand.palm_normal, hand.palm_velocity))
        print ",%s,id,%d,hand_position,%s,hand_direction,%s,hand_normal,%s,hand_velocity,%s" % (
            handType, hand.id, hand.palm_position, hand.direction, hand.palm_normal, hand.palm_velocity)

        if len(frame.hands) == 2:
            if hand.is_left:
                hand_left_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000,
                                      hand.palm_position.z / 1000]
                hand_left_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000,
                                      hand.palm_velocity.z / 1000]
            else:
                hand_right_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000,
                                       hand.palm_position.z / 1000]
                hand_right_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000,
                                       hand.palm_velocity.z / 1000]
        elif len(frame.hands) == 1:
            hand_left_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000, hand.palm_position.z / 1000]
            hand_left_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000, hand.palm_velocity.z / 1000]
            hand_right_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000,
                                   hand.palm_position.z / 1000]
            hand_right_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000,
                                   hand.palm_velocity.z / 1000]
    if len(frame.hands) == 0:
        hand_left_position = [0.2, 0, 0]
        hand_left_velocity = [0, 0, 0]
        hand_right_position = [-0.2, 0, 0]
        hand_right_velocity = [0, 0, 0]

    middle_hand = [(hand_left_position[0] + hand_right_position[0]) / 2,
                   (hand_left_position[1] + hand_right_position[1]) / 2,
                   (hand_left_position[2] + hand_right_position[2]) / 2]
    if len(frame.hands) == 2:
        angle_tray = np.arctan(
            (hand_left_position[1] - hand_right_position[1]) / (hand_left_position[0] - hand_right_position[0]))
    else:
        angle_tray = 0

    msg_pack_in, address = sock_out.recvfrom(1024)  # receving request from port
    msg_out = struct.pack("f f f f f f f f f f f f f f f f",
                          hand_left_position[0], hand_left_position[1], hand_left_position[2],
                          # position of the first hand
                          hand_left_velocity[0], hand_left_velocity[1], hand_left_velocity[2],
                          # velocity of the first hand
                          hand_right_position[0], hand_right_position[1], hand_right_position[2],
                          # position of the second hand
                          hand_right_velocity[0], hand_right_velocity[1], hand_right_velocity[2],
                          # velocity of the second hand
                          middle_hand[0], middle_hand[1], middle_hand[2],  # middle of the hands
                          angle_tray)  # angle of rotation in radian
    sock_out.sendto(msg_out, address)