# Insert Trial Schedule Here
# from simple_settings.py import *

# Insert Trial Schedule Above
# Start Experiment after the following trial number

from H3DInterface import *
from H3D import *
from math import (ceil, floor, atan, sqrt, sin, cos, pow, atan2)
import numpy as np

## Variables for Phase and Trial Settings

global Target_Positions, Rest_Positions, number_targets, time_of_trial
Rest_Positions = np.genfromtxt('rest_position.txt', delimiter = ',')
Target_Positions = np.genfromtxt('target_position.txt', delimiter = ',')
number_targets = len(Target_Positions)

casereq_last = -1
casereq = 0
Trial_random_pick = 0
CaseTimer = 0
time_of_trial = 4 # seconds

ExperimentComplete = 0

global file_is_open, filename
file_is_open = 0
filename = 'RenameMe_Data.txt'


def initialize():
    # initialize vectors of length(trials) for settings
    print "initialize"
    global Target_Positions
    global TimeInitial

    TimeInitial = references.getValue()[0].cycleTime.getValue()
    Header = "Initial time %s\n Target position %s\n" %(TimeInitial, Target_Positions)
    data_labels = "time   TrialCounter  Casereq    Left_x  Left_y  Left_z  Right_x Right_y Right_z Tray_x  Tray_y  Tray_z  Tray_rot_x  Tray_rot_y  Tray_rot_z  Tray_angle  Sphere_x    Sphere_y   Sphere_z"
    f = open(filename, 'a')
    f.write('%s \n' % (data_labels))
    f.close()
    return 0


class State_Detection(AutoUpdate(TypedField(MFBool, (SFTime)))):
    def update(self, event):

        global TimeInitial
        routes_in = self.getRoutesIn()
        time = routes_in[0].getValue()
        time = time - TimeInitial
        # H3D relative 
        TimeSensor = references.getValue()[0]
        View_Point = references.getValue()[1].position.getValue()
        # Actual measurements of the leap
        Leap_left_transl = references.getValue()[2].translation.getValue()
        Leap_right_transl = references.getValue()[3].translation.getValue()
        Velocity_left = references.getValue()[6].translation.getValue()
        Velocity_right = references.getValue()[7].translation.getValue()
        # Target point position
        Target_left_pos = references.getValue()[4].translation.getValue()
        Target_right_pos = references.getValue()[5].translation.getValue()
        # Tray features
        Tray_pos = references.getValue()[8].translation.getValue()
        Tray_rot = references.getValue()[8].rotation.getValue()

        Speed_left = sqrt(pow(Velocity_left.x, 2)
                          + pow(Velocity_left.y, 2)
                          + pow(Velocity_left.z, 2))
        Speed_right = sqrt(pow(Velocity_right.x, 2)
                           + pow(Velocity_right.y, 2)
                           + pow(Velocity_right.z, 2))

        Target_Error_left = sqrt(pow(Leap_left_transl.x - Target_left_pos.x, 2)
                                 + pow(Leap_left_transl.y - Target_left_pos.y, 2)
                                 + pow(Leap_left_transl.z - Target_left_pos.z, 2))
        Target_Error_right = sqrt(pow(Leap_right_transl.x - Target_right_pos.x, 2)
                                  + pow(Leap_right_transl.y - Target_right_pos.y, 2)
                                  + pow(Leap_right_transl.z - Target_right_pos.z, 2))

        StateCode = [(Target_Error_left < 0.01) & (Target_Error_right < 0.01),  # 0 reach the target
                     (Target_Error_left > 0.01) & (Target_Error_right > 0.01),  # 1 in cycle reaching condition
                     (Speed_left < 0.1) & (Speed_right < 0.1)]  # 2 No motion

        return StateCode


class Protocol_Control(AutoUpdate(TypedField(SFInt32, (MFBool)))):  # protocol
    def update(self, event):
        ## State information calculated

        routes_in = self.getRoutesIn()
        StateCode = routes_in[0].getValue()

        In_Target = StateCode[0]
        In_Movement = StateCode[1]
        No_Motion = StateCode[2]


        ## Time variable definitions
        TimeSensor = references.getValue()[0]
        global TimeInitial
        time = TimeSensor.cycleTime.getValue()
        Instruction = references.getValue()[9]

        ## Functional varaibles definitions
        global casereq
        global casereq_last
        global Trial_random_pick
        global number_targets
        global ExperimentComplete

        ######## Check if the time is runing
        def case_initial():  # case 0
            if time < 0:
                casereq = 0
            else:
                print "------ Session started -------"
                casereq = 1
            return casereq

        ########
        def case_New_Trial():  # case 1
            global TrialCounter
            Instruction.string.setValue(["Reach the", "target"])
            TrialCounter = TrialCounter + 1
            print "Start of the trial : %d" % (TrialCounter)
            casereq = 2
            return casereq

        ########
        def case_In_Reaching():  # case 2
            global CaseTimer
            CaseTimer = CaseTimer + 1
            if (In_Target & No_Motion):
                    casereq = 1
            else:
                    casereq = 2
            return casereq

        ########
        def case_end():  # case 3
            print "End of the trials"
            Instruction.string.setValue(["Congrats","Session finished"])
            casereq = 3
            return casereq

        ####### Begining of the real process
        pcase = {0: case_initial,
                 1: case_New_Trial,
                 2: case_In_Reaching,
                 3: case_end}

        casereq = pcase[casereq]()
        print casereq
        print
        if TrialCounter == number_trials + 1:
            casereq = 3
            TrialCounter = 1
            ExperimentComplete = 1
        casereq_last = casereq
        return casereq


class Set_Target_Position(TypedField(SFVec3f, (SFInt32))):
    def update(self, event):
        global Trial_random_pick
        global tar_x
        global tar_y
        global tar_z
        routes_in = self.getRoutesIn()
        case = routes_in[0].getValue()
        px = Target_Positions[TrialCounter - 1, 0]
        py = Target_Positions[TrialCounter - 1, 1]
        pz = Target_Positions[TrialCounter - 1, 2]

        if case == 2:
            Target_left_pos = Vec3f(px, py, pz)
            Target_right_pos = Vec3f(px + 0.2, py, pz)
        else:
            Target_left_pos = Vec3f(10, 10, 10)
            Target_right_pos = Vec3f(10, 10, 10)
        references.getValue()[4].translation.setValue(Target_left_pos)
        references.getValue()[5].translation.setValue(Target_right_pos)

        return Target_left_pos


class Write_Data(AutoUpdate(TypedField(SFBool, (SFInt32)))):
    def update(self, event):
        global TimeInitial, file_is_open, filename, Trial_random_pick, f

        routes_in = self.getRoutesIn()
        casereq = routes_in[0].getValue()

        # H3D relative
        time = references.getValue()[0].cycleTime.getValue()

        time = time - TimeInitial
        View_Point = references.getValue()[1].position.getValue()
        # Actual measurements of the leap
        Leap_left_transl = references.getValue()[2].translation.getValue()
        Leap_right_transl = references.getValue()[3].translation.getValue()
         # Target point position
        Target_left_pos = references.getValue()[4].translation.getValue()
        Target_right_pos = references.getValue()[5].translation.getValue()
        # Tray features
        Tray_pos = references.getValue()[8].translation.getValue()
        Tray_rot = references.getValue()[8].rotation.getValue()
        Sphere_pos = references.getValue()[10].translation.getValue()

        if casereq ==1 and file_is_open != 1:
            f = open(filename, 'a')
            print f
            file_is_open = 1

        if casereq ==4 and file_is_open == 1:
            f.close()
            file_is_open = 0

        if casereq != 0 and Leap_left_transl.x != 0 and casereq != 4 and TrialCounter != 0:
            ## labels: "time   TrialCounter     Casereq    Left_x  Left_y  Left_z  Right_x Right_y Right_z Tray_x  Tray_y  Tray_z  Tray_rot_x  Tray_rot_y  Tray_rot_z  Tray_angle  Sphere_x    Sphere_y   Sphere_z"
            # f = open(filename, 'a')
            f.write('%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n' % (time, TrialCounter, casereq,
                                                  Leap_left_transl.x, Leap_left_transl.y, Leap_left_transl.z,
                                                  Leap_right_transl.x,Leap_right_transl.y,Leap_right_transl.z,
                                                  Tray_pos.x, Tray_pos.y, Tray_pos.z,
                                                  Tray_rot.x,Tray_rot.y,Tray_rot.z,Tray_rot.angle,
                                                  Sphere_pos.x, Sphere_pos.y, Sphere_pos.z))
            # f.close()
        return 0


state_detection = State_Detection()
protocol = Protocol_Control()
target_position = Set_Target_Position()

write_data=Write_Data()
