from socket import *
import struct
from H3DInterface import *

global port_out
print "receiver port %f" %port_out
HOST, PORT = "127.0.0.2", port_out
sock_in = socket(AF_INET, SOCK_DGRAM)
message='request'

def initialize():
    pass


def traverseSG():
    Hand_left,Velocity_left,Hand_right,Velocity_right,Tray,time = references.getValue()

    sock_in.sendto(message, (HOST, PORT)) #asks for one piece of data per scene graph loop cycle
    data, addr=sock_in.recvfrom(1024)
    numOfValues = len(data) / 4
    unp=struct.unpack('@' + 'f' * numOfValues, data)


    #For LookingGlass H3D workspace
    Hand_left.getField("translation").setValue(Vec3f (unp[0], unp[1], unp[2] ))
    Velocity_left.getField("translation").setValue(Vec3f (unp[3], unp[4], unp[5] ))
    Hand_right.getField("translation").setValue(Vec3f (unp[6], unp[7], unp[8] ))
    Velocity_right.getField("translation").setValue(Vec3f (unp[9], unp[10], unp[11] ))
    Tray.getField("rotation").setValue(Rotation (0, 0, 1, unp[15] ))
    Tray.getField("translation").setValue(Vec3f (unp[12], unp[13], unp[14]))