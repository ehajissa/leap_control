# Insert Trial Schedule Here
# from simple_settings.py import *
import sys
print(sys.version)
# Insert Trial Schedule Above
# Start Experiment after the following trial number
print "protocol"
from H3DInterface import *
from H3D import *
from math import (ceil, floor, atan, sqrt, sin, cos, pow, atan2, radians)
import numpy as np
import struct, time

## Reciever
from socket import *
port_number=int(np.genfromtxt('port_number.txt', delimiter =',')[1]) #get the port number that was set by Conroller_Leap.py
print "PORT NUMBER"
print port_number
HOST, PORT = "127.0.0.2", port_number
sock_in = socket(AF_INET, SOCK_DGRAM)
message='request'
print "pro port %s" %PORT

### Writing error messges to a file
error_log=open('error.log','w')
sys.stderr=error_log


global Target_Positions, Rest_Positions, number_targets, max_trial_time, success_count


import datetime
Subject_ID = "Unknown"
Date = datetime.datetime.now()
Rest_Positions = np.genfromtxt('workspace/rest_position.txt', delimiter = ',')
width_hand_rest =  Rest_Positions[1, 0] - Rest_Positions[0, 0]
print width_hand_rest
Target_Positions = np.genfromtxt('workspace/targets_on_sphere.txt', delimiter = ',')
number_targets = len(Target_Positions)
print "Target_Positions " + str(Target_Positions) +'end' + '\n len' + str(number_targets)
Error_Augmentation_ON = True
EA_factor = np.genfromtxt('workspace/parameters.txt', delimiter = ',')[0,1]
Target_factor = np.genfromtxt('workspace/parameters.txt', delimiter = ',')[1,1]
max_session_time=45*60 #45 minutes 
block_length = np.genfromtxt('workspace/parameters.txt', delimiter = ',')[3,1] # minutes
block_length = 10#block_length*60 #seconds 
max_trial_time = np.genfromtxt('workspace/parameters.txt', delimiter = ',')[4,1] # seconds
side_to_treat = np.genfromtxt('workspace/parameters.txt', delimiter = ',',dtype=str)[5,1] # String [none,left,right]

if side_to_treat == "left":
    Rest_Positions[1,0],Rest_Positions[1,1],Rest_Positions[1,2] = Rest_Positions[0,0]+width_hand_rest,Rest_Positions[0,1],Rest_Positions[0,2]
elif side_to_treat == "right":
    Rest_Positions[0, 0] , Rest_Positions[0, 1], Rest_Positions[0, 2] = Rest_Positions[1,0] - width_hand_rest,Rest_Positions[1,1],Rest_Positions[1,2]
else:
    Rest_Positions[0, 1] = np.mean([Rest_Positions[0, 1],Rest_Positions[1, 1]]) #average left y and right y
    Rest_Positions[1, 1] = np.mean([Rest_Positions[0, 1],Rest_Positions[1, 1]]) #average left y and right y
    Rest_Positions[0, 2] = np.mean([Rest_Positions[0, 2], Rest_Positions[1, 2]]) #average left z and right z
    Rest_Positions[1, 2] = np.mean([Rest_Positions[0, 2], Rest_Positions[1, 2]]) #average left z and right z

### Calibration file reading
calibration = np.genfromtxt('calibration.txt', delimiter=',')
print calibration

# import os
# sys.path.append(os.getcwd())
# import Coordinate_transformation
# Leap_euler_angles,Leap_translation = np.loadtxt('calibration.txt', delimiter=',', usecols=[1,2,3])
# Leap_euler_angles = [45,0,0] # degree 
# Leap_translation = [0.2, 0, 0] # meter
# Calculates Rotation Matrix given euler angles.
#Leap_transformation = Coordinate_transformation.eulerAnglesToRotationMatrix(Leap_euler_angles,Leap_translation)
# print Leap_transformation

casereq_last = -1
casereq = 0
random_target = 0
trial_count = 0
success_count = 0
error_time=0


##  Specification Data for data recording
global file_is_open, data_path
file_is_open = 0
datapath_filepath='resources/datapath_filepath.txt'
file_path=open(datapath_filepath,'r')
data_path=file_path.read()
file_path.close()

def initialize():
    # initialize vectors of length(trials) for settings
    print "initialize"
    print 
    global Target_Positions, TimeInitial,Target_Zone_Size,Cursor_Radius,Target_Zone_Size_Difference,rest_timer,block_trial_count,block_success_count,rest_count
    TimeInitial = references.getValue()[0].cycleTime.getValue()
    Rotation_screen = references.getValue()[1]
    Translation_screen = references.getValue()[13]
    Cursor_Radius= references.getValue()[2].getField('scale').getValue().x* 0.02 #0.02 is set in ../../inlines/jack_shape.wrl 
    rest_timer=0
    block_trial_count=0
    block_success_count=0
    rest_count=0
    ## Set the width of the tray according to the resting position of the participant
    scaling_tray = Vec3f(width_hand_rest, 1, 1)
    references.getValue()[10].scale.setValue(scaling_tray)
    Target_Zone_Size_Difference = float(np.genfromtxt('workspace/parameters.txt', delimiter = ',')[2,1]) # meters
    Target_Zone_Size= - Target_Zone_Size_Difference + Cursor_Radius # The target sphere radius is some subtraction than the cursor radius
    references.getValue()[14].radius.setValue(Target_Zone_Size) 
    Header = "Subject = %s \nDate = %s \nTarget position = %s" %(Subject_ID, Date, Target_Positions)
    data_labels = "time\ttrial_count\tcasereq\tsuccess_count\tLeap_left_transl.x\tLeap_left_transl.y\tLeap_left_transl.z\tLeap_right_transl.x\tLeap_right_transl.y\tLeap_right_transl.z\tTarget_left_pos.x\tTarget_left_pos.y\tTarget_left_pos.z\tTarget_right_pos.x\tTarget_right_pos.y\tTarget_right_pos.z\tTray_pos.x\tTray_pos.y\tTray_pos.z\tTray_rot.x\tTray_rot.y\tTray_rot.z\tTray_rot.angle\tSphere_pos.x\tSphere_pos.y\tSphere_pos.z\treaching_complete"
    f = open(data_path, 'a')
    f.write('%s \n' % (Header))
    f.write('%s \n' % (data_labels))
    f.close()
    ### Use of the calibration
    Rotation_screen.getField("rotation").setValue(Rotation(1, 0, 0, radians(calibration[0, 4]))) #indices are [label, x, y,z,angle]
    Translation_screen.getField("translation").setValue(Vec3f(calibration[1, 1], calibration[1, 2], calibration[1, 3])) #indices are [label, x, y, z]
    print "Calibration done"
    return 0


class State_Detection(AutoUpdate(TypedField(MFBool, (SFTime)))):
    def update(self, event):
        global TimeInitial, Target_Zone_Size,Cursor_Radius,Target_Zone_Size_Difference, Leap_left_transl, Leap_right_transl
        routes_in = self.getRoutesIn()
        time = routes_in[0].getValue()
        time = time - TimeInitial
        # H3D relative 
        TimeSensor = references.getValue()[0]
        # Actual measurements of the leap
        Leap_left_transl = references.getValue()[2].translation.getValue()
        Leap_right_transl = references.getValue()[3].translation.getValue()
        Velocity_left = references.getValue()[8].translation.getValue()
        Velocity_right = references.getValue()[9].translation.getValue()
        # Home point position
        Home_left_pos = references.getValue()[4].translation.getValue()
        Home_right_pos = references.getValue()[5].translation.getValue()
        # Target point position
        Target_left_pos = references.getValue()[6].translation.getValue()
        Target_right_pos = references.getValue()[7].translation.getValue()
        # Tray features
        Tray_pos = references.getValue()[10].translation.getValue()
        Tray_rot = references.getValue()[10].rotation.getValue()

        Speed_left = sqrt(pow(Velocity_left.x, 2)
                          + pow(Velocity_left.y, 2)
                          + pow(Velocity_left.z, 2))
        Speed_right = sqrt(pow(Velocity_right.x, 2)
                           + pow(Velocity_right.y, 2)
                           + pow(Velocity_right.z, 2))

        Home_Error_left = sqrt(pow(Leap_left_transl.x - Home_left_pos.x, 2)
                               + pow(Leap_left_transl.y - Home_left_pos.y, 2)
                               + pow(Leap_left_transl.z - Home_left_pos.z, 2))
        Home_Error_right = sqrt(pow(Leap_right_transl.x - Home_right_pos.x, 2)
                                + pow(Leap_right_transl.y - Home_right_pos.y, 2)
                                + pow(Leap_right_transl.z - Home_right_pos.z, 2))

        Target_Error_left = sqrt(pow(Leap_left_transl.x - Target_left_pos.x, 2)
                                 + pow(Leap_left_transl.y - Target_left_pos.y, 2)
                                 + pow(Leap_left_transl.z - Target_left_pos.z, 2))
        Target_Error_right = sqrt(pow(Leap_right_transl.x - Target_right_pos.x, 2)
                                  + pow(Leap_right_transl.y - Target_right_pos.y, 2)
                                  + pow(Leap_right_transl.z - Target_right_pos.z, 2))
        
        Percent_Inside= Target_Zone_Size*0.7 #percent overlap required for successful reach
        StateCode = [(Home_Error_left < Percent_Inside),# 0 In_Home_left 
                     (Home_Error_right < Percent_Inside), # 1 In_Home_right 
                     

                     (Home_Error_left > 0.01) & (Home_Error_right > 0.01)
                     & (Target_Error_left > Percent_Inside) & (Target_Error_right > Percent_Inside),
                     # 2 In_Movement

                     (Speed_left < 0.6) & (Speed_right < 0.6),
                     # 3 No_Motion

                     (Target_Error_left < Percent_Inside), # 4 In_Target_left
                     (Target_Error_right < Percent_Inside)]# 5 In_Target_right
                    
        return StateCode


class Protocol_Control(AutoUpdate(TypedField(SFInt32, (MFBool)))):  # protocol
    def update(self, event):
        ## State information calculated
        tray_position = references.getValue()[10].translation.getValue()
        tray_rotation = references.getValue()[10].rotation
        sphere_position= references.getValue()[12].translation.getValue()
        sphere_velocity = references.getValue()[13].translation
        routes_in = self.getRoutesIn()
        StateCode = routes_in[0].getValue()


        In_Home_left = StateCode[0]
        In_Home_right = StateCode[1]
        In_Home= In_Home_left & In_Home_right
        In_Movement = StateCode[2]
        No_Motion = StateCode[3]
        In_Target_left = StateCode[4]
        In_Target_right = StateCode[5]
        In_Target= In_Target_left & In_Target_right

        ## Time variable definitions
        TimeSensor = references.getValue()[0]
        global TimeInitial
        ## Functional varaibles definitions
        global casereq
        global casereq_last
        #global success_count
        global random_target
        global number_targets
        global trial_start_time
        global session_start_time
        global rest_time_length,rest_entry_time
        global reaching_complete,success_count,rest_timer
        global target_hold_time, home_entry_time, exp_time, block_length,error_time, level_check,block_trial_count,block_success_count,rest_count
        target_hold_time=0.5
        cycale_time = TimeSensor.cycleTime.getValue()
        #print "error_time" + str(error_time)
        exp_time = cycale_time - TimeInitial-error_time
        reaching_complete = 0
        
        #change the target's color when inside
        if In_Target_left:
            references.getValue()[15].diffuseColor.setValue(RGB(1,0,0))
        else:
            references.getValue()[15].diffuseColor.setValue(RGB(0,1,0))
        if In_Target_right:
            references.getValue()[18].diffuseColor.setValue(RGB(1,0,0))
        else:
            references.getValue()[18].diffuseColor.setValue(RGB(0,1,0))
        if In_Home_left:
            references.getValue()[19].diffuseColor.setValue(RGB(1,0,0))
        else:
            references.getValue()[19].diffuseColor.setValue(RGB(0,1,0))
        if In_Home_right:
            references.getValue()[20].diffuseColor.setValue(RGB(1,0,0))
        else:
            references.getValue()[20].diffuseColor.setValue(RGB(0,1,0))
        #print "mod (exp_time" + str(exp_time) + "block_length" + str(block_length) + ")  =" + str(np.mod(exp_time, block_length))
        #print "exp_time" +str(exp_time)
       #take a break every 5 minutes regaurdless of current case
        if round(exp_time)>0 and np.mod(exp_time, block_length)<.0166666 and casereq != 0 and casereq !=5: #0166
            print "BREAK"
            rest_timer=0
            level_check=1
            casereq = 5
            rest_entry_time = exp_time
            rest_count=rest_count+1

        ######## Check if the time is running
        def case_initial():  # case 0
            global session_start_time,success_count
            if exp_time < 0:
                casereq = 0
            else:
                print "------ set of trials started -------"
                session_start_time = exp_time
                casereq = 1
            return casereq

        ########
        def case_New_Trial():  # case 1
            global trial_count
            global trial_start_time
            global home_entry_time
            #references.getValue()[15].diffuseColor.setValue( RGB(0,1,0)) #green for GO to home
            if In_Home:
                home_entry_time=exp_time
                casereq = 7
            else:
                casereq = 1
            return casereq

            #******************************************not in use ******************************
        def case_Retry_Trial():  # case 2
            global random_target
            global trial_start_time
            if (In_Home):
                ## No Update of the Trial since we want to retry it
                print "Restart of the trial : %d" % (random_target)
                casereq = 3
                trial_start_time = exp_time
            else:
                casereq = 2
            return casereq
 
            #******************************************not in use ******************************

        def case_In_Reaching():  # case 3
            global CaseTimer
            global breakevent
            global random_target
            global reaching_complete
            global trial_count
            global trial_timer, trial_start_time, rest_entry_time,target_entry_time, max_trial_time

            #references.getValue()[15].diffuseColor.setValue( RGB(0,1,0)) #green for GO to target
            if sphere_position.y < -0.5:
                casereq = 1
                print "Ball was lost :("
            elif exp_time-trial_start_time < max_trial_time:
                if (In_Target):
                    target_entry_time=exp_time
                    casereq = 6 
                else:
                    casereq = 3
            else:
                casereq = 1
                print "Time for trial exceeded :| "
            return casereq

        ########
        def case_In_Target(): #case 6
            global target_entry_time, success_count,target_hold_time, trial_end_time,block_success_count
            time_in_target=exp_time-target_entry_time
            #print "Time in Target" + str(time_in_target)
            #references.getValue()[15].diffuseColor.setValue( RGB(1,0,0)) #red for STOP (pause) in target           
            intensity=time_in_target/target_hold_time 
            references.getValue()[15].ambientIntensity.setValue(intensity)
            if time_in_target>target_hold_time:
                reaching_complete = 1 # = 0 otherwise
                success_count = success_count+1
                block_success_count = block_success_count+1
                print "Reach succeeded  :) " + str(success_count)
                trial_end_time = exp_time
                casereq=1
            elif not (In_Target): #back to case In Reaching if no longer in target
                casereq=3
            else:
                casereq=6 #keep the case if still in target
            return casereq
            
        def case_In_Home(): #case 7
            global home_entry_time, success_count,random_target,trial_count,target_hold_time, trial_start_time, exp_time,block_trial_count
            time_in_home=exp_time-home_entry_time
            #references.getValue()[15].diffuseColor.setValue( RGB(1,0,0)) #red for STOP (pause) in home
            if time_in_home>target_hold_time:                                                  #**visited home, marks the start of a new trial**
                random_target = np.random.randint(0, number_targets ) #once visted home, pick a new target
                trial_count = trial_count+1
                block_trial_count = block_trial_count+1
                #print "Trial Started TargetID:%d" % (random_target)
                trial_start_time = exp_time
                casereq = 3
            elif not (In_Home):  #go back to "new trail" (reset the begining of the trial) if not still in home              
                casereq=1
            else:
                casereq=7 #keep the case if still in home but have not met req to pause in home
            return casereq
                

        def case_end():  # case 4
            casereq = 4
            return casereq

        def case_In_rest(): # case 5
            global rest_entry_time
            global rest_time_length, rest_timer,rest_count
            rest_timer = exp_time - rest_entry_time
            #print "rest_timer "+ str(rest_timer)
            if rest_count==4:
                rest_time_length=10
            else:
                rest_time_length=2
            if rest_timer < rest_time_length:
                casereq = 5
            else:
                casereq = 1
            level_check=0
            return  casereq

        ####### Begining of the real process
        pcase = {0: case_initial,
                 1: case_New_Trial,
                 2: case_Retry_Trial,
                 3: case_In_Reaching,
                 4: case_end,
                 5: case_In_rest,
                 6: case_In_Target,
                 7: case_In_Home}
        #print casereq
        casereq = pcase[casereq]()
        #if exp_time - home_entry_time >
      
        if exp_time - session_start_time > max_session_time:
            casereq = 4
        #print "casereq"+str(casereq)
        casereq_last = casereq

        return casereq

 
class Set_Hand_Position(AutoUpdate(TypedField(SFInt32, (SFInt32)))):
    def update(self, event):

        Hand_left = references.getValue()[2]
        Velocity_left = references.getValue()[8]
        Hand_right = references.getValue()[3]
        Velocity_right = references.getValue()[9]
        

        ## Data exchange with Controller_Leap.py
        sock_in.sendto(message, (HOST, PORT))  # asks for one piece of data per scene graph loop cycle
        data, addr = sock_in.recvfrom(1024)
        numOfValues = len(data) / 4
        unp_data = struct.unpack('@' + 'f' * numOfValues, data)
        
        hand_left_position = np.array(unp_data[0:3])
        hand_left_velocity = unp_data[3:6]
        hand_right_position = np.array(unp_data[6:9])
        hand_right_velocity = unp_data[9:12]
        leaps_message =unp_data[13]
        
        
        
        if EA_factor != 0 :
            
            if side_to_treat == "right":
                inst_error_vector = hand_right_position -(hand_left_position + [width_hand_rest, 0, 0])
                hand_right_position = hand_right_position + EA_factor * inst_error_vector

            elif side_to_treat == "left":
                ## Computation of the instantaneous error hand position error with the left hand as reference
                inst_error_vector = hand_left_position - (hand_right_position - [width_hand_rest, 0, 0])
                hand_left_position = hand_left_position +  EA_factor * inst_error_vector
            
            #this is weird. If you don't choose a side to treat you can't decide which side is "wrong" therefor removed. cannot do EA this way without choosing a side
            # elif side_to_treat == "none": 
                # inst_error_vector = (hand_right_position - hand_left_position) - [width_hand_rest, 0, 0]
                # hand_left_position = hand_left_position - 0.5*EA_factor * inst_error_vector
                # hand_right_position = hand_right_position + 0.5*EA_factor * inst_error_vector

       
        

        Hand_left.getField("translation").setValue(Vec3f(hand_left_position[0],hand_left_position[1],hand_left_position[2]))
        Velocity_left.getField("translation").setValue(Vec3f(hand_left_velocity[0],hand_left_velocity[1],hand_left_velocity[2]))

        Hand_right.getField("translation").setValue(Vec3f(hand_right_position[0],hand_right_position[1],hand_right_position[2]))
        Velocity_right.getField("translation").setValue(Vec3f(hand_right_velocity[0],hand_right_velocity[1],hand_right_velocity[2]))

        return 1

class Set_Tray_Transform(AutoUpdate(TypedField(SFBool, (SFInt32)))):
    def update(self, event):
        angle_tray=0
        Tray = references.getValue()[10]
        Leap_left_transl = references.getValue()[2].translation.getValue()
        Leap_right_transl = references.getValue()[3].translation.getValue()

        middle_hand = [(Leap_left_transl.x + Leap_right_transl.x) / 2,
                       (Leap_left_transl.y + Leap_right_transl.y) / 2,
                       (Leap_left_transl.z + Leap_right_transl.z) / 2]
        
        if (Leap_right_transl.z - Leap_left_transl.z) != 0:
            angle_tray =   np.arctan((Leap_right_transl.y - Leap_left_transl.y) / (Leap_right_transl.x - Leap_left_transl.x))
        

        Tray.getField("rotation").setValue(Rotation(0, 0, 1, angle_tray))
        Tray.getField("translation").setValue(Vec3f(middle_hand[0],middle_hand[1],middle_hand[2]))
        #print "protocol" + str(angle_tray)
        return 1
        
class Set_Target_Position(AutoUpdate(TypedField(SFInt32, (SFInt32)))):
    def update(self, event):
        global random_target

        routes_in = self.getRoutesIn()
        case = routes_in[0].getValue()
        #print "Taget_Factor" + str(Target_factor)
        [px, py, pz ] = Target_Positions[random_target]*Target_factor

        [px, py, pz] = [px + Rest_Positions[0, 0],
                        py + Rest_Positions[0, 1],
                        pz + Rest_Positions[0, 2]]

        if casereq == 3 or casereq == 6 :
            Target_left_pos = Vec3f(px, py, pz)
            Target_right_pos = Vec3f(px + width_hand_rest, py, pz)
        else:
            Target_left_pos = Vec3f(10, 10, 10)
            Target_right_pos = Vec3f(10, 10, 10)

        references.getValue()[6].translation.setValue(Target_left_pos)
        references.getValue()[7].translation.setValue(Target_right_pos)
        return 1


class Set_Home_Position(AutoUpdate(TypedField(SFInt32, (SFInt32)))):
    def update(self, event):
        routes_in = self.getRoutesIn()
        case = routes_in[0].getValue()
        Leap_left_transl = references.getValue()[2].translation.getValue()
        Leap_right_transl = references.getValue()[3].translation.getValue()
        
        global random_target, max_trial_time, success_count,trial_count, exp_time, trial_start_time,error_time,Target_factor,level_check, block_trial_count,block_success_count,rest_count
        Home_left_pos = references.getValue()[4]
        Home_right_pos = references.getValue()[5]
        Instruction = references.getValue()[11]
        Instruction_Position = references.getValue()[16]
                #change the message if the not valid hands
        #print "error?" +str(Leap_left_transl.x in [30,20,15,10])
        if Leap_left_transl.x in [30,20,15,10] and casereq != 5:
            error_time=error_time+0.016666
        
        if casereq not in [5,4]:        
            if Leap_left_transl.x == 30:
                Instruction.string.setValue(["Too many hands are in view."])
            elif Leap_left_transl.x == 20:
                Instruction.string.setValue(["Please show your hands."])
            elif Leap_left_transl.x == 15:
                Instruction.string.setValue(["Please adjust LEFT hand."])
            elif Leap_right_transl.x == 10:
                Instruction.string.setValue(["Please adjust RIGHT hand."])  
            else:
                if case in [0, 1, 2, 7]:
                    Home_left_pos.translation.setValue(Vec3f(Rest_Positions[0, 0],
                                                             Rest_Positions[0, 1],
                                                             Rest_Positions[0, 2]))
                    Home_right_pos.translation.setValue(Vec3f(Rest_Positions[1,0],
                                                              Rest_Positions[1,1],
                                                              Rest_Positions[1, 2]))

                    Instruction.string.setValue(["Reach to the", "starting position"])
                    Instruction_Position.translation.setValue(Vec3f(0,0,0))
                elif case in [3, 6]:
                    Home_left_pos.translation.setValue(Vec3f(10, 10, 10))
                    Home_right_pos.translation.setValue(Vec3f(10, 10, 10))
                    trial_countdown = np.round(max_trial_time-(exp_time-trial_start_time))
                    Instruction_Position.translation.setValue(Vec3f(0,.125,0))
                    Instruction.string.setValue(["Reach to the target ", str(trial_countdown)])
                    
        elif casereq == 5: 
            if level_check is 1: #level check is a flag that goes 0->1 once for each rest period allowing the level to only change once per rest period.
                if block_trial_count is not 0:
                    sucess_ratio = float(block_success_count) / block_trial_count
                    print "sucess_ratio" + str(sucess_ratio)
                    #print "Target_factor" + str(Target_factor)
                    if sucess_ratio >= 0.7:
                        level_update = ["Great Job!\n", "You've Leveled-UP"]
                        Target_factor=Target_factor+0.02
                    elif sucess_ratio <  float(1)/3:
                        level_update = ["The task might be too hard.\n","Level Decreased"]
                        Target_factor=Target_factor-0.02
                    elif float(1)/3 <= sucess_ratio <0.7 :
                        level_update = ["Nice Job!\n","Let's do more practice."]
                else:
                    sucess_ratio=0
                    level_update=[]    
                block_trial_count=0
                block_success_count=0
                level_check=0
                Instruction_Position.translation.setValue(Vec3f(0,0,0))
                Instruction.string.setValue(level_update + ["\n Take a rest for now :)"] )
                Home_left_pos.translation.setValue(Vec3f(100, 100, 100))
                Home_right_pos.translation.setValue(Vec3f(100, 100, 100))
                if rest_count == 4: 
                    Instruction.string.setValue(level_update + ["\n Take a 10 minute break :)"] )
        else: #ergo, caseqreq=4, the end case
            Instruction_Position.translation.setValue(Vec3f(0,0,0))
            Instruction.string.setValue(["All Done!"])
        
        return 1


class Write_Data(AutoUpdate(TypedField(SFBool, (SFInt32)))):
    def update(self, event):

        global TimeInitial, file_is_open, data_path, random_target, f, reaching_complete
        routes_in = self.getRoutesIn()
        casereq = routes_in[0].getValue()

        # H3D relative
        time = references.getValue()[0].cycleTime.getValue()

        time = time - TimeInitial
        # Actual measurements of the leap
        Leap_left_transl = references.getValue()[2].translation.getValue()
        Leap_right_transl = references.getValue()[3].translation.getValue()
        Velocity_left = references.getValue()[8].translation.getValue()
        Velocity_right = references.getValue()[9].translation.getValue()
        # Home point position
        Home_left_pos = references.getValue()[4].translation.getValue()
        Home_right_pos = references.getValue()[5].translation.getValue()
        # Target point position
        Target_left_pos = references.getValue()[6].translation.getValue()
        Target_right_pos = references.getValue()[7].translation.getValue()
        # Tray features
        Tray_pos = references.getValue()[10].translation.getValue()
        Tray_rot = references.getValue()[10].rotation.getValue()
        Sphere_pos = references.getValue()[12].translation.getValue()
        
        if casereq ==1 and file_is_open != 1:
            f = open(data_path, 'a')
            file_is_open = 1

        if casereq ==4 and file_is_open == 1:
            f.close()
            file_is_open = 0
 
        f.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (time, trial_count, casereq, success_count,
                                                                                Leap_left_transl.x, Leap_left_transl.y, Leap_left_transl.z,
                                                                                Leap_right_transl.x, Leap_right_transl.y, Leap_right_transl.z,
                                                                                Target_left_pos.x,Target_left_pos.y,Target_left_pos.z,
                                                                                Target_right_pos.x,Target_right_pos.y,Target_right_pos.z,
                                                                                Tray_pos.x, Tray_pos.y, Tray_pos.z,
                                                                                Tray_rot.x, Tray_rot.y, Tray_rot.z, Tray_rot.angle,
                                                                                Sphere_pos.x, Sphere_pos.y, Sphere_pos.z,
                                                                                reaching_complete))
        
            # f.close()
        return 0


state_detection = State_Detection()
protocol = Protocol_Control()

hand_positions = Set_Hand_Position()
tray_transform=Set_Tray_Transform()
target_position = Set_Target_Position()
home_position = Set_Home_Position()

write_data=Write_Data()
