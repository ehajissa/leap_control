from H3DInterface import *
import numpy as np   
import time, os
import winsound as ws
global g, friction
global Old_time, Old_velocity, Old_position_perp, Old_position

Old_time = 0.0
Old_velocity = 0.0
Old_position_perp = 0.0
Old_position = Vec3f(0,0,0)
print Old_position

## Simulation of the physic
g = 9
friction = 1
angle_limit = 0.05
fall_enable = True
Rest_Positions = np.genfromtxt('workspace/rest_position.txt', delimiter = ',')
friction = np.genfromtxt('workspace/parameters.txt', delimiter = ',')[6,1]
print "Friction : %f" %friction
width_hand_rest = Rest_Positions[1, 0] - Rest_Positions[0, 0]
water_drip_path=str(os.getcwd()) + '\sounds\drop.wav'
print water_drip_path
class Set_Sphere_Position(AutoUpdate(TypedField( SFVec3f,(SFInt32) )) ):
    def update(self, event):
        global g, friction
        global Old_time, Old_velocity, Old_position_perp, Old_position

        routes_in = self.getRoutesIn()
        case = routes_in[0].getValue()

        ## Reference values definition
        sphere_balance, tray = references.getValue()

        tray_position = tray.translation.getValue()+Vec3f(0,.025,0)
        tray_rot = -tray.rotation.getValue().angle
        Actual_time = time.time()
        time_inc = Actual_time - Old_time
        addZ=0 #0.06
        if case == 3 or case == 6:

            ## Columbic friction condition on movement, with gravity and viscous effects
            if abs(tray_rot) < angle_limit:
                acc_perp_tray = - friction*Old_velocity
            else:
                acc_perp_tray = - g*np.sin(tray_rot)*np.cos(tray_rot) - friction*Old_velocity

            ## Integration
            vel_perp_tray = Old_velocity + acc_perp_tray*time_inc
            pos_perp_tray = Old_position_perp

            if pos_perp_tray < -(width_hand_rest/2):
                if fall_enable == True:
                    return_ball_position = Vec3f(Old_position.x,
                                                 Old_position.y - 0.01,
                                                 Old_position.z )
                    ws.PlaySound(water_drip_path,ws.SND_ASYNC)    
                else:
                    pos_perp_tray = -(width_hand_rest/2)
                    vel_perp_tray = 0
                    return_ball_position = Vec3f(tray_position.x - pos_perp_tray * np.cos(tray_rot),
                                                 tray_position.y + pos_perp_tray * np.sin(tray_rot),
                                                 tray_position.z )

            elif pos_perp_tray > (width_hand_rest/2):
                if fall_enable == True:
                    return_ball_position = Vec3f(Old_position.x,
                                                 Old_position.y - 0.01,
                                                 Old_position.z)
                    ws.PlaySound(water_drip_path,ws.SND_ASYNC)
                else:
                    pos_perp_tray = (width_hand_rest/2)
                    vel_perp_tray = 0
                    return_ball_position = Vec3f(tray_position.x - pos_perp_tray * np.cos(tray_rot),
                                                 tray_position.y + pos_perp_tray * np.sin(tray_rot),
                                                 tray_position.z )
            else:
                pos_perp_tray = ((1/2)*acc_perp_tray*time_inc*time_inc)+ vel_perp_tray*time_inc + pos_perp_tray
                return_ball_position = Vec3f(tray_position.x - pos_perp_tray*np.cos(tray_rot),
                                             tray_position.y + pos_perp_tray*np.sin(tray_rot),
                                             tray_position.z )
            # print "Old Time:", Old_time
            # print "Actual time", Actual_time
            # print "Increment  ", time_inc
            # print "Done"
            Old_time = Actual_time
            Old_velocity = vel_perp_tray
            Old_position_perp = pos_perp_tray
        else:
            Old_time = Actual_time
            Old_velocity = 0.0
            Old_position_perp = 0.0
            return_ball_position = Vec3f(tray_position.x,
                                         tray_position.y,
                                         tray_position.z + 0)

        Old_position = return_ball_position
        sphere_balance.getField("translation").setValue(return_ball_position)
        return return_ball_position

       
    
sphere_position = Set_Sphere_Position() 
