GUI.py calls
Tasks/2D_reaching_2D_ball/Controller_leap.py
controller_leap.py starts a loop that gets updated every time there's a new Leap frame
a leap frame is updated every time there's new leap data coming in over a UDP channel
This loop runs faster than 60Hz, possibly as fast as the leap's output at 120Hz.
Controller_leap.py calls visualization.x3d which runs several py files:
- protocol.py
- Sphere_physic.py
visualization draws all objects

protocol has:
- the state machine
- writes the data
- updates position of cursor based on leap (coming in over UDP)
- grabs info from several setup text files to fix calibration and specific experiment parameters like reach distance and others these are loaded from:
	- root folder has calibration.txt
	- workspaces folder inside root contains parameters.txt and rest_position.txt