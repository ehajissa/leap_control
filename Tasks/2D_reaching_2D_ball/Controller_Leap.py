import os, sys, inspect,  struct, win32process, Leap
import numpy as np
print sys.executable
#set up socket communication
from socket import *
sock_out = socket(AF_INET, SOCK_DGRAM)
sock_out.bind(('', 0)) #0 slects a random available port
port_out=sock_out.getsockname()[1] #get the port number to send to the receiver
port_file = open("port_number.txt", 'w') #open (and make a new file) for writing
port_file.write("port number,%s" %str(port_out))
port_file.close()

### Writing error messges to a file
error_log=open('controller_error.log','w')
sys.stderr=error_log

#*** change the path to import a custom library
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))
sys.path.append(os.getcwd())
import Coordinate_transformation #custom lib

# Leap_euler_angles = [2.155,0,0] # radians from Leap To TV ==> Desktop mode
# Leap_translation = [0, 0.35, 0.55] # meter  y for the height z for the depth
# Leap_euler_angles = [0.785,0,0] # radians from Leap To TV
# Leap_translation = [0, 0.4, -0.3] # meter  y for the height z for the depth
# Calculates Rotation Matrix given euler angles.

# **START H3D with specified windows process settings **
root_dir = os.getcwd() # Open the x3d file (NB The default x3d file program should be H3DViewer bin32)
print "root_dir" + str(root_dir)
#os.startfile(root_dir+'/Tasks/2D_reaching_2D_ball/visualization.x3d')
process_name=root_dir+'/Tasks/2D_reaching_2D_ball/visualization.x3d' 
si=win32process.STARTUPINFO()
si.dwFlags = win32process.STARTF_USEPOSITION #flag to use dwX and dwY
si.dwX = 0 #*
si.dwY = -1080 # together put window in upper left of TV screen (if Display Settings is setup properly)
# params=dict()
# params['startupinfo'] = si
processSecurityAttributes = None
threadSecurityAttributes = None
fInheritHandles = 0
creationFlags = win32process.CREATE_DEFAULT_ERROR_MODE #makes python scripts called by H3D output errors to the cmd
environment = None
currentDirectory = None
win32process.CreateProcess(None, 'H3DLoad '+process_name, None, None, 1, creationFlags,None, None ,si)
#new version: allows more control with crateprocess such as setting the window position. All createprocess arguments are required
#pervoiusly: subprocess.Popen(["H3DLoad", process_name],**params)#this works better than startfile because it outputs child process error messages

# ** Setup Leap Settings and Start Loop to send its data on UDP **
Leap_transformation = Coordinate_transformation.eulerAnglesToRotationMatrix()
print Leap_transformation
controller = Leap.Controller()
controller.set_policy(Leap.Controller.POLICY_BACKGROUND_FRAMES) ## Enable background apps for the Leap
import Coordinate_transformation_inverse #custom lib
Leap_transformation_I = Coordinate_transformation_inverse.eulerAnglesToRotationMatrix()
leap_message='valid hands in view'
while True:

    frame = controller.frame()
    Hands = frame.hands
    if len(Hands) == 2:
        for hand in Hands:
             
            # hand_positionT = np.dot(Leap_transformation, [hand.palm_position.x / 1000, hand.palm_position.y / 1000, hand.palm_position.z / 1000, 1])
            # hand_velocityT = np.dot(Leap_transformation, [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000, hand.palm_velocity.z / 1000, 0])
            
            # hand_position =[hand.palm_position.x / 1000, hand.palm_position.y / 1000, hand.palm_position.z / 1000, 1]
            # hand_velocity =[hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000, hand.palm_velocity.z / 1000, 0]
            # hand_positionI=np.dot(Leap_transformation_I,hand_positionT)
            # print "T " + str(hand_positionT)+"\n"
            # print "I " + str(hand_positionI)+"\n"
            # print "O " + str(hand_position)+"\n"

            hand_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000, hand.palm_position.z / 1000, 1]
            hand_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000, hand.palm_velocity.z / 1000, 0]

            if Hands.leftmost==hand: #.leftmost gives the hand id of the leftmost hand:
                hand_left_position = hand_position
                hand_left_velocity = hand_velocity
            else:
                hand_right_position = hand_position
                hand_right_velocity = hand_velocity
                    
    elif len(frame.hands) == 1: #when you only see one hand then the cursors disappear offscreen x=10
        hand=frame.hands[0]
        hand_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000, hand.palm_position.z / 1000, 1]
        hand_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000, hand.palm_velocity.z / 1000, 0]
        if frame.hands[0].is_left:
            hand_left_position = hand_position
            hand_left_velocity = hand_velocity
            hand_right_position =[10, 0, 0, 1]
            hand_right_velocity =[10, 0, 0, 0]
            leap_message='Please adjust RIGHT hand.'
        else: 
            hand_left_position =[15, 0, 0, 1]
            hand_left_velocity = [15, 0, 0, 0]
            hand_right_position =hand_position
            hand_right_velocity = hand_velocity
            leap_message='Please adjust LEFT hand.'
    elif len(frame.hands) > 2: #if too many hands seen then the cursors disappear offscreen x=30
        hand_left_position =[30, 0, 0, 1]
        hand_left_velocity = [30, 0, 0, 0]
        hand_right_position =[30, 0, 0, 1]
        hand_right_velocity =[30, 0, 0, 0] 
        leap_message='Too many hands are in view.'
    else: #if no hands are seen then the cursors disappear offscreen x=20
        hand_left_position =[20, 0, 0, 1]
        hand_left_velocity = [20, 0, 0, 0]
        hand_right_position =[20, 0, 0, 1]
        hand_right_velocity =[20, 0, 0, 0]
        leap_message='Please Show your hands.'

    middle_hand = [(hand_left_position[0] + hand_right_position[0]) / 2,
                   (hand_left_position[1] + hand_right_position[1]) / 2,
                   (hand_left_position[2] + hand_right_position[2]) / 2]
    

    if len(frame.hands) == 2:
        angle_tray = -  np.arctan((hand_right_position[2] - hand_left_position[2]) / (hand_right_position[0] - hand_left_position[0]))
    else:
        angle_tray = 0
    #print str(hand_left_position[0])+str(hand_left_position[1])+str(hand_left_position[2])
    
    #ERROR: IOError: [Errno 22] invalid mode ('w') or filename: 'resources/hands_in_view.txt' write the leap message about which hands are in view to a file
        # print "BEFORE WRITE"
        # hands_in_view_file = open("resources/hands_in_view.txt", 'w') #open (and make a new file) for writing
        # hands_in_view_file.write(leap_message)
        # hands_in_view_file.close()
        # print "AFTER WRITE"
    
    msg_pack_in, address = sock_out.recvfrom(1024)  # receving request from port
    msg_out = struct.pack("f f f f f f f f f f f f f f f f",
                          hand_left_position[0], hand_left_position[1], hand_left_position[2],
                          # position of the first hand
                          hand_left_velocity[0], hand_left_velocity[1], hand_left_velocity[2],
                          # velocity of the first hand
                          hand_right_position[0], hand_right_position[1], hand_right_position[2],
                          # position of the second hand
                          hand_right_velocity[0], hand_right_velocity[1], hand_right_velocity[2],
                          # velocity of the second hand
                          middle_hand[0], middle_hand[1], middle_hand[2],  # middle of the hands
                          angle_tray)  # angle of rotation in radian
    sock_out.sendto(msg_out, address)
    
    #print str([hand_left_position[0], hand_left_position[1], hand_left_position[2]])