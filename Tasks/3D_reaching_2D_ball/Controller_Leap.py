import os, sys, inspect, thread, time, struct
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
import numpy as np

## Open the x3d file (NB The default x3d file program should be H3DViewer bin32)
root_dir = os.getcwd()
os.startfile(root_dir+'/Tasks/3D_reaching_2D_ball/visualization.x3d')

sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))
import Leap

from socket import *
PORT_out = 12000
remoteHost_out = ''
sock_out = socket(AF_INET, SOCK_DGRAM)
sock_out.bind(('', PORT_out))

class SampleListener(Leap.Listener):

    def on_init(self, controller):
        print "Initialized"
    def on_connect(self, controller):
        print "Connected"
    def on_disconnect(self, controller):
        print "Disconnected"
    def on_exit(self, controller):
        print "Exited"
    def on_frame(self, controller):
        print "Frame available"


controller = Leap.Controller()
listener = SampleListener()
# Have the sample listener receive events from the controller
# controller.add_listener(listener)


count = 0
while True:
    count = count + 1
    if np.mod(count,2) ==1:
        print
    else:
        pass

    frame = controller.frame()
    Hands = frame.hands
    # file.write("Frame_id,%d,timestamp,%d,hands,%d,fingers,%d" % (
    #     frame.id, frame.timestamp, len(frame.hands), len(frame.fingers)))

    print "Frame_id,%d,timestamp,%d,hands,%d,fingers,%d" % (
        frame.id, frame.timestamp, len(frame.hands), len(frame.fingers))

    for hand in frame.hands:

        handType = "left" if hand.is_left else "right"
        normal = hand.palm_normal
        direction = hand.direction
        arm = hand.arm

        # file.write(",%s,id,%d,hand_position,%s,hand_direction,%s,hand_normal,%s,hand_velocity,%s" % (
        #     handType, hand.id, hand.palm_position, hand.direction, hand.palm_normal, hand.palm_velocity))
        print ",%s,id,%d,hand_position,%s,hand_direction,%s,hand_normal,%s,hand_velocity,%s" % (
            handType, hand.id, hand.palm_position, hand.direction, hand.palm_normal, hand.palm_velocity)

        if len(frame.hands) == 2:
            if hand.is_left:
                hand_left_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000,
                                      hand.palm_position.z / 1000]
                hand_left_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000,
                                      hand.palm_velocity.z / 1000]
            else:
                hand_right_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000,
                                       hand.palm_position.z / 1000]
                hand_right_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000,
                                       hand.palm_velocity.z / 1000]
        elif len(frame.hands) == 1:
            hand_left_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000, hand.palm_position.z / 1000]
            hand_left_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000, hand.palm_velocity.z / 1000]
            hand_right_position = [hand.palm_position.x / 1000, hand.palm_position.y / 1000,
                                   hand.palm_position.z / 1000]
            hand_right_velocity = [hand.palm_velocity.x / 1000, hand.palm_velocity.y / 1000,
                                   hand.palm_velocity.z / 1000]
    if len(frame.hands) == 0:
        hand_left_position = [0.2, 0, 0]
        hand_left_velocity = [0, 0, 0]
        hand_right_position = [-0.2, 0, 0]
        hand_right_velocity = [0, 0, 0]

    middle_hand = [(hand_left_position[0] + hand_right_position[0]) / 2,
                   (hand_left_position[1] + hand_right_position[1]) / 2,
                   (hand_left_position[2] + hand_right_position[2]) / 2]
    if len(frame.hands) == 2:
        angle_tray = np.arctan(
            (hand_left_position[1] - hand_right_position[1]) / (hand_left_position[0] - hand_right_position[0]))
    else:
        angle_tray = 0

    msg_pack_in, address = sock_out.recvfrom(1024)  # receving request from port
    msg_out = struct.pack("f f f f f f f f f f f f f f f f",
                          hand_left_position[0], hand_left_position[1], hand_left_position[2],
                          # position of the first hand
                          hand_left_velocity[0], hand_left_velocity[1], hand_left_velocity[2],
                          # velocity of the first hand
                          hand_right_position[0], hand_right_position[1], hand_right_position[2],
                          # position of the second hand
                          hand_right_velocity[0], hand_right_velocity[1], hand_right_velocity[2],
                          # velocity of the second hand
                          middle_hand[0], middle_hand[1], middle_hand[2],  # middle of the hands
                          angle_tray)  # angle of rotation in radian
    sock_out.sendto(msg_out, address)

# def traverseSG():
#     Hand_left,Velocity_left,Hand_right,Velocity_right,Tray,time = references.getValue()
#
#     sock_in.sendto(message, (HOST, PORT)) #asks for one piece of data per scene graph loop cycle
#     data, addr=sock_in.recvfrom(1024)
#     numOfValues = len(data) / 4
#     unp=struct.unpack('@' + 'f' * numOfValues, data)
#
#     #For LookingGlass H3D workspace
#     Hand_left.getField("translation").setValue(Vec3f (unp[0], unp[1], unp[2] ))
#     Velocity_left.getField("translation").setValue(Vec3f (unp[3], unp[4], unp[5] ))
#     Hand_right.getField("translation").setValue(Vec3f (unp[6], unp[7], unp[8] ))
#     Velocity_right.getField("translation").setValue(Vec3f (unp[9], unp[10], unp[11] ))
#     Tray.getField("rotation").setValue(Rotation (0, 0, 1, unp[15] ))
#     Tray.getField("translation").setValue(Vec3f (unp[12], unp[13], unp[14]))